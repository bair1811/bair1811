package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import biblioteca.util.Validator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestCtrl {

    private CartiRepoInterface cartiRepository=new CartiRepoMock();

    @PostMapping("/save")
    public boolean validate(@RequestBody Carte carte){
        System.out.println("aaaaaaaaaaaaaaaaaaa");
        cartiRepository.adaugaCarte(carte);
        try {
            Validator.validateCarte(carte);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
