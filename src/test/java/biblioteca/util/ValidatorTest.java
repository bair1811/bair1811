package biblioteca.util;

import biblioteca.model.Carte;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ValidatorTest {
    @Test
    public void TC1_ECP() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Title");
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("altcuvant");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(true);
        }catch (Exception e){
            assert(false);
        }

    }

    @Test
    public void TC2_ECP() throws Exception {
        Carte c = new Carte();
        c.setTitlu("T1tle");
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autorr");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("altcuvant");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(false);
        }catch (Exception e){
            assert(e.getMessage().equals("Titlu invalid!"));
        }

    }

    @Test
    public void TC3_ECP() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Title");
        List<String> ref = new ArrayList<String>();
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("altcuvant");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(false);
        }catch (Exception e){
            assert(e.getMessage().equals("Lista autori vida!"));
        }

    }

    @Test
    public void TC4_ECP() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Title");
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("O mie");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("altcuvant");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(false);
        }catch (Exception e){
            assert(e.getMessage().equals("An aparitie invalid!"));
        }
    }

    @Test
    public void TC5_ECP() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Title");
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("cuvant2");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(false);
        }catch (Exception e){
            assert(e.getMessage().equals("Cuvant cheie invalid!"));
        }
    }

    @Test
    public void TC2_BVA() throws Exception {
        Carte c = new Carte();
        c.setTitlu("M");
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("altcuvant");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(true);
        }catch (Exception e){
            assert(false);
        }

    }

    @Test
    public void TC3_BVA() throws Exception {
        Carte c = new Carte();

        String titlu = "";
        for(int i=0;i<255;i++)
            titlu = titlu.concat("M");

        c.setTitlu(titlu);
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("altcuvant");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(true);
        }catch (Exception e){
            assert(false);
        }

    }



    @Test
    public void TC4_BVA() throws Exception {
        Carte c = new Carte();

        String titlu = "";
        for(int i=0;i<254;i++)
            titlu = titlu.concat("M");

        c.setTitlu(titlu);
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("cuvant");
        cuv.add("altcuvant");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(true);
        }catch (Exception e){
            assert(false);
        }

    }

    @Test
    public void TC7_BVA() throws Exception {
        Carte c = new Carte();
        c.setTitlu("titlu");
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add("K");
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(true);
        }catch (Exception e){
            assert(false);
        }

    }

    @Test
    public void TC8_BVA() throws Exception {
        Carte c = new Carte();

        String kw = "";
        for(int i=0;i<255;i++)
            kw = kw.concat("K");

        c.setTitlu("titlu");
        List<String> ref = new ArrayList<String>();
        ref.add("Autor");
        ref.add("Autor");
        c.setReferenti(ref);
        c.setAnAparitie("1999");
        c.setEditura("Erc");
        List<String> cuv = new ArrayList<String>();
        cuv.add(kw);
        c.setCuvinteCheie(cuv);

        try{
            Validator.validateCarte(c);
            assert(true);
        }catch (Exception e){
            assert(false);
        }

    }


}