package biblioteca.integration;

import biblioteca.repository.repo.CartiRepoTest;
import biblioteca.util.ValidatorTest;
import org.junit.Test;

public class BigBand {
    @Test
    public void unit1() throws Exception {
        ValidatorTest valTest = new ValidatorTest();
        valTest.TC1_ECP();
        valTest.TC2_BVA();
        valTest.TC2_ECP();
        valTest.TC3_ECP();
        valTest.TC3_BVA();
        valTest.TC4_BVA();
        valTest.TC4_ECP();
        valTest.TC5_ECP();
        valTest.TC7_BVA();
        valTest.TC8_BVA();
    }

    @Test
    public void unit2() throws Exception {
        CartiRepoTest ctest = new CartiRepoTest();
        ctest.TC1();
        ctest.TC2();
    }

    @Test
    public void unit3() throws Exception {
        CartiRepoTest ctest = new CartiRepoTest();
        ctest.TC1C3();
        ctest.TC2C3();
    }

    @Test
    public void integration() throws Exception {
        unit1();
        unit2();
        unit3();
    }
}
