package biblioteca.repository.repo;

import biblioteca.model.Carte;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoTest {
    @Test
    public void TC1() throws Exception {
        CartiRepo repo = new CartiRepo("out/tc1.txt");
        List<Carte> found = repo.cautaCarte("Eminescu2");
        assert(found.size()==0);
    }
    @Test
    public void TC2() throws Exception {
        CartiRepo repo = new CartiRepo("out/tc2.txt");
        List<Carte> found = repo.cautaCarte("Eminescu");
        assert(found.size()==2);
    }
    @Test
    public void TC1C3() throws Exception {
        CartiRepo repo = new CartiRepo("out/tc2.txt");
        List<Carte> found = repo.getCartiOrdonateDinAnul("1989");
        assert(found.size()==2);
    }
    @Test
    public void TC2C3() throws Exception {
        CartiRepo repo = new CartiRepo("out/tc2.txt");
        List<Carte> found = repo.getCartiOrdonateDinAnul("aaaaaa");
        assert(found.size()==0);
    }
}