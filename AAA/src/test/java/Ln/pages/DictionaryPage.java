package Ln.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:8080/altceva.html")
public class DictionaryPage extends PageObject {
    @FindBy(name="titlu")
    private WebElementFacade titlu;
    @FindBy(name="autor")
    private WebElementFacade autor;
    @FindBy(name="anAparitie")
    private WebElementFacade anAparitie;
    @FindBy(name="editura")
    private WebElementFacade editura;
    @FindBy(name="cuvCheie")
    private WebElementFacade cuvCheie;


    @FindBy(name="btn")
    private WebElementFacade lookupButton;

    public void entertitlu(String keyword) {
        titlu.type(keyword);
    }
    public void enterautor(String keyword) {
        autor.type(keyword);
    }
    public void enteranAparitie(String keyword) {
        anAparitie.type(keyword);
    }
    public void entereditura(String keyword) {
        editura.type(keyword);
    }
    public void entercuvCheie(String keyword) {
        cuvCheie.type(keyword);
    }


    public void lookup_terms() {
        lookupButton.click();
    }

    public String getValue() {
        WebElementFacade definitionList = find(By.id("Rezultat"));
        return definitionList.getValue();
    }
}