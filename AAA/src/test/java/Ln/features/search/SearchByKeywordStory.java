package Ln.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import Ln.steps.serenity.EndUserSteps;

@RunWith(SerenityRunner.class)
public class SearchByKeywordStory {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    @Issue("#Form-1")
    @Test
    public void addInvalidEmployee() {
        anna.is_the_home_page();
        anna.entertitlu("Hell2o");
        anna.enteranAparitie("123455");
        anna.enterautor("Assitent");
        anna.entereditura("World");
        anna.entercuvCheie("123455");
        anna.starts_search();
        Assert.assertEquals("Nu s-a trimis cum trebuie", anna.getValue());
    }
    @Test
    public void addValidEmployee() {
        anna.is_the_home_page();
        anna.entertitlu("AAAAAA");
        anna.enteranAparitie("1");
        anna.enterautor("AAAAAAAAAA");
        anna.entereditura("AAAAA");
        anna.entercuvCheie("AAAAAAA");
        anna.starts_search();
        Assert.assertEquals("Ai adaugat cu succes!", anna.getValue());
    }
} 